const body = document.body;
const menu = document.querySelector('.menu');
const menuTogleBtn = document.querySelector('.menu-btn');
const menuTogleBtnLines = [...document.querySelectorAll('.menu-btn__line')];

body.addEventListener('click', e => {
	const target = e.target;
	if (target === menuTogleBtn || target.classList.contains('menu-btn__line')) {
		menu.classList.toggle('menu-is-open');
	} else {
		menu.classList.remove('menu-is-open');
	}
	toggleMenu();
});

function toggleMenu() {
	if (menu.classList.contains('menu-is-open')) {
		menuTogleBtnLines.forEach(line => {
			line.classList.add('menu-btn__line--open');
		});
	} else {
		menuTogleBtnLines.forEach(line => {
			line.classList.remove('menu-btn__line--open');
		});
	}
}


// Price active
// const listPrice = document.querySelector('.sect-price__list');
// listPrice.addEventListener('click', showActivePrice);


// function showActivePrice(e) {
// 	let target = e.target;
// 	if (target.closest('.sect-price__item')) {
// 		document.querySelectorAll('.sect-price__item').forEach(e => e.classList.remove('item--active'));
// 		target.closest('.sect-price__item').classList.add('item--active')

// 	}
// }

